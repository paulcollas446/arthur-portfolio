<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'arthur' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ' zS?`1{ch(O~M0e/FOO8L0AF*/_aL4ZL,dd>Bx:V;wIKUj<m,G5iv8Q7%NE|&yf9' );
define( 'SECURE_AUTH_KEY',  'r|GT2]#X?LU1mNL3AMiQv&YC=RVnl>yc|7J{==$EiGcGETONl@$%zu0XU{mW?@*)' );
define( 'LOGGED_IN_KEY',    'AJ6OyX64|usQPrk0=?;+*V$(/w<I QxDBwpE;dnKDly=y,6}c,Y(BLG9/c)>gDm&' );
define( 'NONCE_KEY',        'tjvo-PP^)A^W1+jwmbP*xy]RL}ED]1p(R/G%.m+ggDI>k#clWxpUFOHi.7KY~YHr' );
define( 'AUTH_SALT',        'fR4Sn7~2`5!ADNVhf_o Cbh>Oy|#63py*g=Ni]m,Y6y@{lcl9k+$gq-!Xe(E +go' );
define( 'SECURE_AUTH_SALT', 'r{:{tR(OsG#$<sU[Bn&Du+18 fSn<UsdHlfxM>x:KCS6pAC3~%S5yC[4SzcufXg%' );
define( 'LOGGED_IN_SALT',   'aL}f}$2ek6vc&Y8k0C{99+)z6iDb_=4O*AG.vOf|_o~bIHL9pD2ccmAwiCwxb]xK' );
define( 'NONCE_SALT',       'q[degUG=xa`ZoFoz]AY(T&-Q;5n][|@kEP t%K2$!{-D-k@0pmUCg0r#iigZpkxh' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
