<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage arthur-portfolio
 * @since arthur-portfolio 1.0
 * @version 1.0
 */

get_header();

get_template_part('template-parts/page/page-slider');
get_template_part('template-parts/page/page-about');
get_template_part('template-parts/page/page-competence');
get_template_part('template-parts/home/home-realisation');
get_template_part('template-parts/page/page-counters');

get_footer();

    
