<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage arthur-portfolio
 * @since arthur-portfolio 1.0
 * @version 1.0
 */

?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <title>Arthur Besnehard | Portfolio - Designer, vidéaste, vidéo, cinéma, print, web</title>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  
  <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,700" rel="stylesheet">

  <link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/assets/css/open-iconic-bootstrap.min.css">
  <link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/assets/css/animate.css">
  
  <link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/assets/css/owl.carousel.min.css">
  <link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/assets/css/owl.theme.default.min.css">
  <link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/assets/css/magnific-popup.css">

  <link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/assets/css/aos.css">

  <link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/assets/css/ionicons.min.css">

  <link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/assets/css/bootstrap-datepicker.css">
  <link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/assets/css/jquery.timepicker.css">

  
  <link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/assets/css/flaticon.css">
  <link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/assets/css/icomoon.css">
  <link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/assets/css/style.css">
  <?php wp_head(); ?>

</head>
<body>
    <div class="KW_progressContainer">
        <div class="KW_progressBar">

        </div>
    </div>
<div class="page">
<?php get_template_part('template-parts/navigation/navigation-nav'); ?>

    <div id="colorlib-page">
<?php

get_template_part('template-parts/navigation/navigation-top');