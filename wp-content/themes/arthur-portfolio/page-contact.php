<?php
/**
 *
 * Template Name: contact
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage arthur-portfolio
 * @since arthur-portfolio 1.0
 * @version 1.0
 */

get_header();

while (have_posts()) : the_post();
    ?>

    <section class="ftco-section contact-section">
        <div class="container mt-5">
            <div class="row d-flex mb-5 contact-info">
                <div class="col-md-12 mb-4">
                    <h2 class="h4">Contactez moi :</h2>
                </div>
                <div class="w-100"></div>
                <?php if (get_field('adresse')): ?>
                    <div class="col">
                        <p><span>Mon adresse : </span><?php the_field("adresse"); ?></p>
                    </div>
                <?php endif; ?>
                <?php if (get_field('phone')): ?>
                    <div class="col">
                        <p><span>Téléphone: </span><a
                                    href="tel:<?php the_field("phone") ?>"><?php the_field("phone") ?></a></p>
                    </div>
                <?php endif; ?>
                <?php if (get_field('email')): ?>
                    <div class="col">
                        <p><span>E-mail: </span> <a
                                    href="mailto:<?php the_field("email") ?>"><?php the_field("email") ?></a></p>
                    </div>
                <?php endif; ?>
            </div>
            <div class="row block-9">
                <div class="col-md-6 pr-md-5">
                    <?php echo do_shortcode('[contact-form-7 id="148"]'); ?>
                </div>
                <div class="col-md-6 mb-5">
                    <iframe width="500" height="440" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
                            src="https://www.openstreetmap.org/export/embed.html?bbox=-1.396636962890625%2C48.98247022887956%2C-0.8232879638671876%2C49.18080571099239&amp;layer=mapnik"
                            style="border: 1px solid black"></iframe>
                    <br/>
                </div>
            </div>
        </div>
    </section>


<?php
endwhile;
get_footer();