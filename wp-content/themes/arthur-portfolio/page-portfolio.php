<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage arthur-portfolio
 * @since arthur-portfolio 1.0
 * @version 1.0
 */

get_header();

$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

$terms = array(
	'post_type'      => 'realisations',
	'posts_per_page' => '6',
	'orderby'        => 'date',
	'order'          => 'DESC',
	'paged'          => $paged
);
$loop  = new WP_Query( $terms );
?>

    <section class="ftco-section">
        <div class="container mt-5">
            <div class="row justify-content-center mb-5 pb-5">
                <div class="col-md-7 text-center heading-section ftco-animate">
                    <span>Portfolio</span>
                    <h2>Checkout a few of my works</h2>
                </div>
            </div>
            <div class="row no-gutters">

				<?php $i = 0;
				while ( $loop->have_posts() ) : $loop->the_post();
					$i ++;
					?>
					<?php if ( $i % 2 != 0 ) : ?>

                        <div class="block-3 d-md-flex ftco-animate" data-scrollax-parent="true">
                            <a href="<?php echo the_permalink(); ?>"
                               class="image d-flex justify-content-center align-items-center"
                               style="background-image: url(<?php the_post_thumbnail_url(); ?>); "
                               data-scrollax=" properties: { translateY: '-30%'}">
                                <div class="icon d-flex text-center justify-content-center align-items-center">
                                    <span class="icon-search"></span>
                                </div>
                            </a>
                            <div class="text">
                                <h4 class="subheading blue"><?php $categories = ' ';
									foreach ( get_the_category() as $category ) {
										$categories .= $category->name . ', ';
									}
									echo substr( $categories, 0, - 2 ); ?></h4>
                                <h2 class="heading"><a href="<?php echo the_permalink(); ?>"><?php the_title(); ?></a>
                                </h2>
                                <p><?php the_excerpt(); ?></p>
                                <p><a href="<?php echo the_permalink(); ?>">Voir plus</a></p>
                            </div>
                        </div>

					<?php else : ?>

                        <div class="block-3 d-md-flex ftco-animate" data-scrollax-parent="true">
                            <a href="<?php echo the_permalink(); ?>"
                               class="image order-2 d-flex justify-content-center align-items-center"
                               style="background-image: url(<?php the_post_thumbnail_url(); ?>); "
                               data-scrollax=" properties: { translateY: '-30%'}">
                                <div class="icon d-flex text-center justify-content-center align-items-center">
                                    <span class="icon-search"></span>
                                </div>
                            </a>
                            <div class="text order-1">
                                <h4 class="subheading blue"><?php $categories = ' ';
									foreach ( get_the_category() as $category ) {
										$categories .= $category->name . ', ';
									}
									echo substr( $categories, 0, - 2 ); ?></h4>
                                <h2 class="heading"><a href="<?php echo the_permalink(); ?>"><?php the_title(); ?></a>
                                </h2>
                                <p><?php the_excerpt(); ?></p>
                                <p><a href="<?php echo the_permalink(); ?>">Voir plus</a></p>
                            </div>
                        </div>

					<?php
					endif;
				endwhile;
				wp_reset_postdata();
				?>

            </div>
            <div class="row mt-5">
                <div class="col text-center">
                    <div class="block-27">
						<?php
						if ( $loop->max_num_pages <= 1 ) {
							return;
						}

						$big = 999999999;

						$pages = paginate_links( array(
							'base'      => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
							'format'    => '?paged=%#%',
							'current'   => max( 1, get_query_var( 'paged' ) ),
							'total'     => $loop->max_num_pages,
							'type'      => 'array',
							'prev_text' => __( '&lt;', 'arthur-portfolio' ),
							'next_text' => __( '&gt;', 'arthur-portfolio' )
						) );
						if ( is_array( $pages ) ) {
							echo '<ul>';
							foreach ( $pages as $page ) {
								echo "<li>$page</li>";
							}
							echo '</ul>';
						}
						?>
                    </div>
                </div>
            </div>
        </div>
    </section>


<?php
get_footer();
