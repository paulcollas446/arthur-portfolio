<!-- Début description -->
<?php
$terms = array(
    'post_type' => 'realisations',
    'posts_per_page' => '4',
    'orderby' => 'date',
    'order' => 'DESC',
);

$loop = new WP_Query($terms);

?>

<section class="ftco-section">
    <div class="container">
        <div class="row justify-content-center mb-5 pb-5">
            <div class="col-md-7 text-center heading-section ftco-animate">
                <span>Portfolio</span>
                <h2>Mes dernières réalisations</h2>
            </div>
        </div>
        <div class="row no-gutters">

            <?php $i = 0;
            while ($loop->have_posts()) : $loop->the_post();
                $i++;
                ?>
                <?php if ($i % 2 != 0) : ?>

                    <div class="block-3 d-md-flex ftco-animate" data-scrollax-parent="true">
                        <a href="<?php echo the_permalink(); ?>"
                           class="image d-flex justify-content-center align-items-center"
                           style="background-image: url(<?php the_post_thumbnail_url(); ?>); "
                           data-scrollax=" properties: { translateY: '-30%'}">
                            <div class="icon d-flex text-center justify-content-center align-items-center">
                                <span class="icon-search"></span>
                            </div>
                        </a>
                        <div class="text">
                            <h4 class="blue subheading"><?php $categories = ' ';
                                foreach (get_the_category() as $category) {
                                    $categories .= $category->name . ', ';
                                }
                                echo substr($categories, 0, -2); ?></h4>
                            <h2 class="heading"><a href="<?php echo the_permalink(); ?>"><?php the_title(); ?></a>
                            </h2>
                            <p><?php the_excerpt(); ?></p>
                            <p><a href="<?php echo the_permalink(); ?>">Voir plus</a></p>
                        </div>
                    </div>

                <?php else : ?>

                    <div class="block-3 d-md-flex ftco-animate" data-scrollax-parent="true">
                        <a href="<?php echo the_permalink(); ?>"
                           class="image order-2 d-flex justify-content-center align-items-center"
                           style="background-image: url(<?php the_post_thumbnail_url(); ?>); "
                           data-scrollax=" properties: { translateY: '-30%'}">
                            <div class="icon d-flex text-center justify-content-center align-items-center">
                                <span class="icon-search"></span>
                            </div>
                        </a>
                        <div class="text order-1">
                            <h4 class="blue subheading"><?php $categories = ' ';
                                foreach (get_the_category() as $category) {
                                    $categories .= $category->name . ', ';
                                }
                                echo substr($categories, 0, -2); ?></h4>
                            <h2 class="heading"><a href="<?php echo the_permalink(); ?>"><?php the_title(); ?></a>
                            </h2>
                            <p><?php the_excerpt(); ?></p>
                            <p><a href="<?php echo the_permalink(); ?>">Voir plus</a></p>
                        </div>
                    </div>

                <?php
                endif;
            endwhile;
            wp_reset_postdata();
            ?>

            <!-- Deuxième version de la cards de la réalisation -->
            <!-- <div class="block-3 d-md-flex ftco-animate" data-scrollax-parent="true">
                <a href="blog-single.html" class="image order-2 d-flex justify-content-center align-items-center" style="background-image: url('images/work-2.jpg'); " data-scrollax=" properties: { translateY: '-30%'}">
                <div class="icon d-flex text-center justify-content-center align-items-center">
                    <span class="icon-search"></span>
                </div>
                </a>
                <div class="text order-1">
                <h4 class="subheading">VIDEO</h4>
                <h2 class="heading"><a href="blog-singleo.html">Réalisation 2</a></h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                <p><a href="blog-single.html">Voir plus</a></p>
                </div>
            </div> -->


        </div>
    </div>
</section>

