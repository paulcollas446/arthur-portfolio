<?php
$terms = array(
	'post_type' => 'about_cpt',
);
$loop  = new WP_Query( $terms );

$image1 = get_field( 'about-image', 34 );
?>
<section class="ftco-section about-section">
    <div class="container" id="about">
        <div class="row d-flex" data-scrollax-parent="true">
            <div class="col-md-4 author-img" style="background-image: url(<?php echo $image1['url']; ?>);"
                 data-scrollax=" properties: { translateY: '-70%'}"></div>
            <div class="col-md-2"></div>
            <div class="col-md-6 wrap ftco-animate">
                <div class="about-desc">
                    <h1 class="bold-text" style="z-index: -1">About</h1>
                    <div class="p-5">
                        <h2 class="mb-5"><?php the_field( 'about-title', 34 ); ?></h2>
                        <p><?php the_field( 'about-description', 34 ); ?></p>
						<?php if ( get_field( 'about_video_link', 34 ) ) : ?><p><a
                                    href="<?php the_field( 'about_video_link', 34 ); ?>"><?php the_field( 'about_video_link_text', 34 ); ?></a>
                            </p><?php endif; ?>
                        <ul class="ftco-footer-social list-unstyled mt-4">
							<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
                                <li><a href="<?php the_field( 'link' ); ?>"><span
                                                class="<?php the_field( 'icon' ); ?>"></span></a></li>
							<?php endwhile;
							wp_reset_postdata();
							?></ul>
                        <h5>Contactez moi</h5>
                        <p>Email: <a
                                    href="mailto:<?php the_field( 'about-email', 34 ); ?>"><?php the_field( 'about-email', 34 ); ?></a>
                        </p>
                        <p>Tel:
                            <a href="tel:<?php the_field( 'about-phone', 34 ); ?>"><?php the_field( 'about-phone', 34 ); ?></a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>