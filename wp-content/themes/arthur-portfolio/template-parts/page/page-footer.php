<?php
$terms = array(
    'post_type' => 'about_cpt',
);
$loop = new WP_Query($terms);
?>
<footer class="ftco-footer ftco-bg-dark ftco-section">
    <div class="container">
        <div class="row mb-5 justify-content-center">
            <div class="col-md-5 text-center">
                <div class="ftco-footer-widget mb-5">
                    <ul class="ftco-footer-social list-unstyled">
                        <?php while ($loop->have_posts()) : $loop->the_post(); ?>
                            <li class="ftco-animate"><a href="<?php the_field('link'); ?>"><span
                                            class="<?php the_field('icon'); ?>"></span></a></li>
                        <?php endwhile;
                        wp_reset_postdata();
                        ?></ul>
                </div>
                <div class="ftco-footer-widget">
                    <h2 class="mb-3">Contactez moi</h2>
                    <p class="h3 email"><a
                                href="mailto:<?php the_field('about-email', 34); ?>"><?php the_field('about-email', 34); ?></a>
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">


            </div>
        </div>
    </div>
</footer>
