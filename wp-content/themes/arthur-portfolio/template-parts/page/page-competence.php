<?php

$terms = array(
	'post_type'      => 'competence',
	'posts_per_page' => '3',
	'orderby'        => 'date',
	'order'          => 'ASC',
);
$loop  = new WP_Query( $terms );

?>

<section class="ftco-section">
    <div class="container">
        <div class="row justify-content-center mb-5 pb-5">
            <div class="col-md-7 text-center heading-section ftco-animate">
                <span>Que fais tu ?</span>
                <h2>Mes compétences</h2>
            </div>
        </div>
        <div class="row">
			<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
                <div class="col d-flex align-self-stretch ftco-animate">
                    <div class="media block-6 services p-3 py-4 d-block text-center">
                        <div class="icon mb-3"><span class="<?php the_field( 'competence-icon' ); ?>"></span></div>
                        <div class="media-body">
                            <h3 class="heading"><?php the_title(); ?></h3>
                            <h3 class="heading"><?php the_field( 'competence-subtitle-1' ); ?></h3>
                            <h3 class="heading"><?php the_field( 'competence-subtitle-2' ); ?></h3>
                        </div>
                    </div>
                </div>
			<?php
			endwhile;
			wp_reset_postdata();
			?>
        </div>
    </div>
</section>