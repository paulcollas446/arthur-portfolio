<section class="ftco-section ftco-counter" id="section-counter">
    <div class="container">
        <div class="row justify-content-center mb-5 pb-5">
            <div class="col-md-7 text-center heading-section ftco-animate">
                <span>Quelques données</span>
                <h2>J'adore dévoiler mes achivements</h2>
            </div>
        </div>
        <div class="row d-flex justify-content-start">
            <div class="col-md-5 col-sm-5 counter-wrap ftco-animate">
                <div class="block-18">
                    <div class="text">
                        <span class="ftco-label"><?php the_field( 'counters-title1', 34 ); ?></span>
                        <strong class="number" data-number="<?php the_field( 'counters-number1', 34 ); ?>">0</strong>
                    </div>
                </div>
            </div>
        </div>
        <div class="row d-flex justify-content-center">
            <div class="col-md-5 col-sm-5 counter-wrap ftco-animate">
                <div class="block-18">
                    <div class="text">
                        <span class="ftco-label"><?php the_field( 'counters-title2', 34 ); ?></span>
                        <strong class="number" data-number="<?php the_field( 'counters-number2', 34 ); ?>">0</strong>
                    </div>
                </div>
            </div>
        </div>
        <div class="row d-flex justify-content-end">
            <div class="col-md-5 counter-wrap ftco-animate">
                <div class="block-18">
                    <div class="text">
                        <span class="ftco-label"><?php the_field( 'counters-title3', 34 ); ?></span>
                        <strong class="number" data-number="<?php the_field( 'counters-number3', 34 ); ?>">0</strong>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>