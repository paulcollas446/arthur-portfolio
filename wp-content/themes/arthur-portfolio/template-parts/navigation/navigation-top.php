<?php

$image1 = get_field( 'image', 237 );

?>
<header>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="colorlib-navbar-brand">
                    <a class="colorlib-logo" href="<?php bloginfo( 'url' ); ?>"><span class="logo-img"
                                                                                      style="background-image: url(<?php echo $image1['url']; ?>);"></span><?php the_field( 'name', 237 ); ?>
                    </a>
                </div>
                <a href="#" class="js-colorlib-nav-toggle colorlib-nav-toggle"><i></i></a>
            </div>
        </div>
    </div>
</header>