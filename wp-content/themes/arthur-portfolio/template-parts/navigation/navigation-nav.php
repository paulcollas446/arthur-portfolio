<?php

$image1 = get_field('image', 248);

?>

<nav id="colorlib-main-nav" role="navigation">
    <a href="#" class="js-colorlib-nav-toggle colorlib-nav-toggle active"><i></i></a>
    <div class="js-fullheight colorlib-table">
        <div class="img" style="background-image: url(<?php echo $image1['url']; ?>);"></div>
        <div class="colorlib-table-cell js-fullheight">
            <div class="row no-gutters">
                <div class="col-md-12 text-center">
                    <h1 class="mb-4"><a href="<?php bloginfo( 'url' ); ?>" class="logo"><?php the_field( 'titre', 248 ); ?></a></h1>
					<?php

					if ( has_nav_menu( 'nav-menu' ) ) {
						wp_nav_menu( array(
							'menu'           => 'nav-menu',
							'theme_location' => 'nav-menu'
						) );
					} else {
						echo 'Veuillez assigner un menu dans l\'administration WordPress -> Apparence -> Menus -> Gérer les emplacements';
					}

					?>
                </div>
            </div>
        </div>
    </div>
</nav>