<?php
/**
 *
 * Template Name: skills
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage arthur-portfolio
 * @since arthur-portfolio 1.0
 * @version 1.0
 */

get_header();


$terms_experiences = array(
	'post_type' => 'experiences',
	'orderby'   => 'date_de_debut',
	'order'     => 'ASC',
);
$loop_experiences  = new WP_Query( $terms_experiences );

$terms_logiciels = array(
	'post_type' => 'logiciels',
	'orderby'   => 'number',
	'order'     => 'DESC',
);
$loop_logiciels  = new WP_Query( $terms_logiciels );

$terms_formations = array(
	'post_type' => 'formations',
	'orderby'   => 'date_de_debut',
	'order'     => 'ASC',
);
$loop_formations  = new WP_Query( $terms_formations );

?>
    <section class="ftco-section about-section">
        <div class="container">
            <div class="row d-flex justify-content-end mt-5">
                <div class="col-md-10">
                    <div class="profile ftco-animate">
                        <h2 class="mb-4">Mon CV</h2>
                        <h4 class="mb-4"><?php the_field( 'cv-subtitle' ); ?></h4>
                        <p><?php the_field( 'cv-text' ); ?></p>
                    </div>

                    <div class="exp mt-5 ftco-animate">
                        <h2 class="mb-4">Mes expériences</h2>

						<?php while ( $loop_experiences->have_posts() ) : $loop_experiences->the_post(); ?>

                            <div class="exp-wrap py-4">
                                <div class="desc">
                                    <h4><?php the_field( 'type_d’emploi' ); ?>
                                        <span> - <?php the_field( 'entreprise' ); ?></span></h4>
									<?php if ( get_field( 'lieu' ) ) : ?><p class="location">
                                        <strong><?php the_field( 'lieu' );
											?></strong>
                                        - <?php endif;
										the_field( 'intitule_du_poste' ); ?></p>
                                </div>
                                <div class="year">

									<?php if ( ! get_field( 'date_de_fin' ) ) : ?>
                                        <p><?php the_field( 'date_de_debut' ); ?>
                                            - Maintenant</p>
									<?php else: ?>
										<?php if ( get_field( 'date_de_debut' ) == get_field( 'date_de_fin' ) ): ?>
                                            <p><?php the_field( 'date_de_debut' ); ?></p>
										<?php else: ?>
                                            <p><?php the_field( 'date_de_debut' ); ?>
                                                - <?php the_field( 'date_de_fin' ); ?></p>
										<?php
										endif;
									endif;
									?>

                                </div>
                            </div>

						<?php endwhile;
						wp_reset_postdata();
						?>

                    </div>

                    <div class="exp mt-5 ftco-animate">
                        <h2 class="mb-4">My Skills</h2>

						<?php while ( $loop_logiciels->have_posts() ) : $loop_logiciels->the_post(); ?>

                            <div class="animate-box">
                                <div class="progress-wrap">
                                    <h4><?php the_field( 'logiciel' ); ?></h4>
                                    <div class="progress">
                                        <div class="progress-bar color-1" role="progressbar"
                                             aria-valuenow="<?php the_field( 'number' ); ?>"
                                             aria-valuemin="0" aria-valuemax="100"
                                             style="width:<?php the_field( 'number' ); ?>%">
                                            <span><?php the_field( 'number' ); ?>%</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

						<?php endwhile;
						wp_reset_postdata();
						?>

                    </div>

                    <div class="exp mt-5 ftco-animate">
                        <h2 class="mb-4">Formations</h2>

						<?php while ( $loop_formations->have_posts() ) : $loop_formations->the_post(); ?>

                            <div class="exp-wrap py-4">
                                <div class="desc">
                                    <h4><?php the_field( 'domaine_d’etudes' ); ?>
                                        <span> - <?php the_field( 'ecole' ); ?></span></h4>
									<?php if ( get_field( 'pays' ) ) : ?><p class="location">
										<?php the_field( 'pays' );
										if ( get_field( 'ville' ) ): ?> - <?php the_field( 'ville' ); endif;
										endif; ?>
                                    </p>
                                </div>
                                <div class="year">

									<?php if ( ! get_field( 'date_de_fin' ) ) : ?>
                                        <p><?php the_field( 'date_de_debut' ); ?>
                                            - Maintenant</p>
									<?php else: ?>
										<?php if ( get_field( 'date_de_debut' ) == get_field( 'date_de_fin' ) ): ?>
                                            <p><?php the_field( 'date_de_debut' ); ?></p>
										<?php else: ?>
                                            <p><?php the_field( 'date_de_debut' ); ?>
                                                - <?php the_field( 'date_de_fin' ); ?></p>
										<?php
										endif;
									endif;
									?>

                                </div>
                            </div>

						<?php endwhile;
						wp_reset_postdata();
						?>

                    </div>
                </div>
            </div>
        </div>
    </section>
<?php
get_footer();