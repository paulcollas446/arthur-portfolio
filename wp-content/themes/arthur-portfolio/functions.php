<?php
/**
 * Twenty Seventeen functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage arthur-portfolio
 * @since arthur-portfolio 1.0
 */

// Empeche l'accès direct au fichier 
defined('ABSPATH') || exit;


// Paramètrage du theme
if (!function_exists('arthurportfolio_setup')) :
    function arthurportfolio_setup()
    {

        // Active le support des images à la une
        add_theme_support('post-thumbnails');

        add_image_size('products', 400, '', array('center', 'center')); // Hard crop left top

        // Active la génération dynamique des title
        add_theme_support('title-tag');

        // Déclaration des zones de menu
        register_nav_menus(
            array(
                'navigation' => 'Navigation principale',
            )
        );


        // Active les formats spéciaux dans les articles
        add_theme_support('post-formats', array('aside', 'image', 'gallery', 'video'));

        // Active le balisage HTML5 sur certaines fonctionnalités
        add_theme_support(
            'html5',
            array(
                'search-form',
                'comment-form',
                'comment-list',
                'gallery',
                'caption',
            )
        );

        register_nav_menus(array(
            'nav-menu' => __('Nav Menu', 'arthur-portfolio'),
        ));

        // Permet le rafraichissement auto des widgets
        add_theme_support('customize-selective-refresh-widgets');

        // Gutenberg Add support for Block Styles.
        add_theme_support('wp-block-styles');

        // Gutenberg Add support for full and wide align images.
        add_theme_support('align-wide');

        // Gutenberg Add support for editor styles.
        add_theme_support('editor-styles');

        // Gutenberg Add support for responsive embedded content.
        add_theme_support('responsive-embeds');
    }
endif;
add_action('after_setup_theme', 'arthurportfolio_setup');


// Chargement des feuilles de style CSS et des scripts JS
function arthurportfolio_scripts()
{

// CSS  
    // STYLE
    wp_enqueue_style('style-arthurportfolio', get_template_directory_uri() . '/assets/css/style.css');

    wp_enqueue_style('style-arthurportfolio');

    // ICOMOON
    wp_enqueue_style('style-icomoon', get_template_directory_uri() . '/assets/css/icomoon.css');

    // FLATICON
    wp_enqueue_style('style-flaticon', get_template_directory_uri() . '/assets/css/flaticon.css');

    // TIMEPICKER
    wp_enqueue_style('style-timepicker', get_template_directory_uri() . '/assets/css/jquery.timepicker.css');

    // DATEPICKER
    wp_enqueue_style('style-datepicker', get_template_directory_uri() . '/assets/css/bootstrap-datepicker.css');

    // IONICONS
    wp_enqueue_style('style-ionicons', get_template_directory_uri() . '/assets/css/ionicons.min.css');

    // AOS
    wp_enqueue_style('style-aos', get_template_directory_uri() . '/assets/css/aos.css');

    // POPUP
    wp_enqueue_style('style-popup', get_template_directory_uri() . '/assets/css/magnific-popup.css');

    // OWLTHEME
    wp_enqueue_style('style-owl', get_template_directory_uri() . '/assets/css/owl.theme.default.min.css');

    // OWLCAROUSSEL
    wp_enqueue_style('style-carousel', get_template_directory_uri() . '/assets/css/owl.carousel.min.css');

    // ANIMATE
    wp_enqueue_style('style-animate', get_template_directory_uri() . '/assets/css/animate.css');

    // OPEN
    wp_enqueue_style('style-iconic', get_template_directory_uri() . '/assets/css/open-iconic-bootstrap.min.css');

    // FANCYBOX
    wp_enqueue_style('jquery-fancybox', get_template_directory_uri() . '/assets/css/jquery.fancybox.min.css');


// JS / JQuery
    // Désactive le jQuery natif de WP
    wp_deregister_script('jquery');

    // JQUERY
    wp_enqueue_script('jquery-arthurportfolio', get_template_directory_uri() . '/assets/js/jquery.min.js');

    // MIGRATE
    wp_enqueue_script('jquery-migrate', get_template_directory_uri() . '/assets/js/jquery-migrate-3.0.1.min.js');

    // POPPER
    wp_enqueue_script('jquery-popper', get_template_directory_uri() . '/assets/js/popper.min.js');

    // BOOSTRAP
    wp_enqueue_script('jquery-bootstrap', get_template_directory_uri() . '/assets/js/bootstrap.min.js');

    // EASING
    wp_enqueue_script('jquery-easing', get_template_directory_uri() . '/assets/js/bootstrap.min.js');

    // WAYPOINTS
    wp_enqueue_script('jquery-waypoints', get_template_directory_uri() . '/assets/js/jquery.easing.1.3.js');

    // STELLAR
    wp_enqueue_script('jquery-stellar', get_template_directory_uri() . '/assets/js/jquery.stellar.min.js');

    // CAROUSEL
    wp_enqueue_script('jquery-carousel', get_template_directory_uri() . '/assets/js/owl.carousel.min.js');

    // MAGNIFIC
    wp_enqueue_script('jquery-magnific', get_template_directory_uri() . '/assets/js/jquery.magnific-popup.min.js');

    // AOS
    wp_enqueue_script('jquery-aos', get_template_directory_uri() . '/assets/js/aos.js');

    // ANIMATENUMBER
    wp_enqueue_script('jquery-animateNumber', get_template_directory_uri() . '/assets/js/jquery.animateNumber.min.js');

    // SCROLLAX
    wp_enqueue_script('jquery-scrollax', get_template_directory_uri() . '/assets/js/scrollax.min.js');

    // DATEPICKER
    wp_enqueue_script('jquery-datepicker', get_template_directory_uri() . '/assets/js/bootstrap-datepicker.js');

    // TIMEPICKER
    wp_enqueue_script('jquery-timepicker', get_template_directory_uri() . '/assets/js/jquery.timepicker.min.js');

    // FANCYBOX
    wp_enqueue_script('jquery-fancybox', get_template_directory_uri() . '/assets/js/jquery.fancybox.min.js');

    // MAIN
    wp_enqueue_script('jquery-googlemap', get_template_directory_uri() . '/assets/js/main.js');


}

add_action('wp_enqueue_scripts', 'arthurportfolio_scripts');

// Retire le margin de 32px d'HTML par défaut

function remove_admin_login_header()
{
    remove_action('wp_head', '_admin_bar_bump_cb');
}

add_action('get_header', 'remove_admin_login_header');


// Modification de la page de connexion WordPress
function my_login_stylesheet()
{
    wp_enqueue_style('custom-login', get_stylesheet_directory_uri() . '/login/login-style.css');
}

add_action('login_enqueue_scripts', 'my_login_stylesheet');

function custom_excerpt_length( $length ) {
    return 18;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

// Appel Custom Post Type
require get_template_directory() . '/inc/cpt-realisations.php';
require get_template_directory() . '/inc/cpt-competences.php';
require get_template_directory() . '/inc/cpt-about.php';
require get_template_directory() . '/inc/cpt-experiences.php';
require get_template_directory() . '/inc/cpt-logiciels.php';
require get_template_directory() . '/inc/cpt-formations.php';


// suppresion partie du menu
function remove_menu_items()
{
    global $menu;
    $restricted = array(__('Article'), __('Comments'), __('Media'), __('Tools'),);
    end($menu);
    while (prev($menu)) {
        $value = explode(' ', $menu[key($menu)][0]);

        if (in_array($value[0] != NULL ? $value[0] : "", $restricted)) {
            unset($menu[key($menu)]);
        }
    }
}

add_action('admin_menu', 'remove_menu_items');

// Supprimer l'éditeur visuel
add_filter('user_can_richedit', create_function('', 'return false;'), 50);

function remove_posts_menu() {
	remove_menu_page('edit.php');
}
add_action('admin_menu', 'remove_posts_menu');