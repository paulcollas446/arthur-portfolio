<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage athur-portfolio
 * @since arthur-portfolio 1.0
 * @version 1.2
 */

?>
</div>
<?php
get_template_part('template-parts/page/page-footer');
?>
<script src="<?php bloginfo( 'template_url' ); ?>/assets/js/jquery.min.js"></script>
<script src="<?php bloginfo( 'template_url' ); ?>/assets/js/jquery-migrate-3.0.1.min.js"></script>
<script src="<?php bloginfo( 'template_url' ); ?>/assets/js/popper.min.js"></script>
<script src="<?php bloginfo( 'template_url' ); ?>/assets/js/bootstrap.min.js"></script>
<script src="<?php bloginfo( 'template_url' ); ?>/assets/js/jquery.easing.1.3.js"></script>
<script src="<?php bloginfo( 'template_url' ); ?>/assets/js/jquery.waypoints.min.js"></script>
<script src="<?php bloginfo( 'template_url' ); ?>/assets/js/jquery.stellar.min.js"></script>
<script src="<?php bloginfo( 'template_url' ); ?>/assets/js/owl.carousel.min.js"></script>
<script src="<?php bloginfo( 'template_url' ); ?>/assets/js/jquery.magnific-popup.min.js"></script>
<script src="<?php bloginfo( 'template_url' ); ?>/assets/js/aos.js"></script>
<script src="<?php bloginfo( 'template_url' ); ?>/assets/js/jquery.animateNumber.min.js"></script>
<script src="<?php bloginfo( 'template_url' ); ?>/assets/js/scrollax.min.js"></script>
<script src="<?php bloginfo( 'template_url' ); ?>/assets/js/bootstrap-datepicker.js"></script>
<script src="<?php bloginfo( 'template_url' ); ?>/assets/js/jquery.timepicker.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
<script src="<?php bloginfo( 'template_url' ); ?>/assets/js/google-map.js"></script>
<script src="<?php bloginfo( 'template_url' ); ?>/assets/js/main.js"></script>


</div>

</body>
</html>