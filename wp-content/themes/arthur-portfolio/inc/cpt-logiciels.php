<?php


// Register Custom Post Type
if ( ! function_exists('alportfolio_cpt')) :
    function alportfolio_cpt() {

        $labels = array(
            'name'                  => _x( 'Logiciels', 'Post Type General Name', 'arthurportfolio' ),
            'singular_name'         => _x( 'logiciels', 'Post Type Singular Name', 'arthurportfolio' ),
            'menu_name'             => __( 'Logiciels', 'arthurportfolio' ),
            'name_admin_bar'        => __( 'Logiciels', 'arthurportfolio' ),
            'archives'              => __( 'Item Archives', 'arthurportfolio' ),
            'attributes'            => __( 'Item Attributes', 'arthurportfolio' ),
            'parent_item_colon'     => __( 'Parent Item:', 'arthurportfolio' ),
            'all_items'             => __( 'Tous les logiciels', 'arthurportfolio' ),
            'add_new_item'          => __( 'Ajouter un logiciel', 'arthurportfolio' ),
            'add_new'               => __( 'Ajouter un logiciel', 'arthurportfolio' ),
            'new_item'              => __( 'Nouveau logiciel', 'arthurportfolio' ),
            'edit_item'             => __( 'Editer un logiciel', 'arthurportfolio' ),
            'update_item'           => __( 'Mettre à jour', 'arthurportfolio' ),
            'view_item'             => __( 'voir logiciel', 'arthurportfolio' ),
            'view_items'            => __( 'voir logiciel', 'arthurportfolio' ),
            'search_items'          => __( 'Rechercher un logiciel', 'arthurportfolio' ),
            'not_found'             => __( 'Aucun logiciel', 'arthurportfolio' ),
            'not_found_in_trash'    => __( 'Aucun logiciel trouvé dans la corbeille', 'arthurportfolio' ),
            'featured_image'        => __( 'Image à la une', 'arthurportfolio' ),
            'set_featured_image'    => __( 'Parametrer Image à la une', 'arthurportfolio' ),
            'remove_featured_image' => __( 'Supprimer Image à la une', 'arthurportfolio' ),
            'use_featured_image'    => __( 'Utiliser Image à la une', 'arthurportfolio' ),
            'insert_into_item'      => __( 'Insert into item', 'arthurportfolio' ),
            'uploaded_to_this_item' => __( 'Uploaded to this item', 'arthurportfolio' ),
            'items_list'            => __( 'Items list', 'arthurportfolio' ),
            'items_list_navigation' => __( 'Items list navigation', 'arthurportfolio' ),
            'filter_items_list'     => __( 'Filter items list', 'arthurportfolio' ),
        );
        $args = array(
            'label'                 => __( 'logiciels', 'arthurportfolio' ),
            'description'           => __( 'CPT pour afficher des fiches des logiciels', 'arthurportfolio' ),
            'labels'                => $labels,
            'supports'              => array( 'title', 'custom-fields' ),
            'hierarchical'          => false,
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_position'         => 5,
            'show_in_admin_bar'     => true,
            'show_in_nav_menus'     => true,
            'can_export'            => true,
            'has_archive'           => true,
            'exclude_from_search'   => false,
            'publicly_queryable'    => true,
            'capability_type'       => 'post',
            'menu_icon'             => 'dashicons-hammer',
            'content'				=> '\0019ff',
            'query_var'             => true,
            'rewrite'               => array( 'slug', 'logiciels' ),
        );
        register_post_type( 'logiciels', $args );
        flush_rewrite_rules();

    }
    add_action( 'init', 'alportfolio_cpt', 10 );
endif;