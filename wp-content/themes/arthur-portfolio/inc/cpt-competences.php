<?php


// Register Custom Post Type
if (!function_exists('aportfolio_cpt')) :
    function aportfolio_cpt()
    {

        $labels = array(
            'name' => _x('Competences', 'Post Type General Name', 'arthurportfolio'),
            'singular_name' => _x('competences', 'Post Type Singular Name', 'arthurportfolio'),
            'menu_name' => __('Compétences', 'arthurportfolio'),
            'name_admin_bar' => __('Competences', 'arthurportfolio'),
            'archives' => __('Item Archives', 'arthurportfolio'),
            'attributes' => __('Item Attributes', 'arthurportfolio'),
            'parent_item_colon' => __('Parent Item:', 'arthurportfolio'),
            'all_items' => __('Toutes les competences', 'arthurportfolio'),
            'add_new_item' => __('Ajouter une competence', 'arthurportfolio'),
            'add_new' => __('Ajouter competence', 'arthurportfolio'),
            'new_item' => __('Nouvelle competence', 'arthurportfolio'),
            'edit_item' => __('Editer une competence', 'arthurportfolio'),
            'update_item' => __('Mettre à jour', 'arthurportfolio'),
            'view_item' => __('voir competence', 'arthurportfolio'),
            'view_items' => __('voir competence', 'arthurportfolio'),
            'search_items' => __('Rechercher une competence', 'arthurportfolio'),
            'not_found' => __('Aucune competence', 'arthurportfolio'),
            'not_found_in_trash' => __('Aucune competence trouvé dans la corbeille', 'arthurportfolio'),
            'featured_image' => __('Image à la une', 'arthurportfolio'),
            'set_featured_image' => __('Parametrer Image à la une', 'arthurportfolio'),
            'remove_featured_image' => __('Supprimer Image à la une', 'arthurportfolio'),
            'use_featured_image' => __('Utiliser Image à la une', 'arthurportfolio'),
            'insert_into_item' => __('Insert into item', 'arthurportfolio'),
            'uploaded_to_this_item' => __('Uploaded to this item', 'arthurportfolio'),
            'items_list' => __('Items list', 'arthurportfolio'),
            'items_list_navigation' => __('Items list navigation', 'arthurportfolio'),
            'filter_items_list' => __('Filter items list', 'arthurportfolio'),
        );
        $args = array(
            'label' => __('competences', 'arthurportfolio'),
            'taxonomies' => array(),
            'description' => __('CPT pour afficher des fiches des competences', 'arthurportfolio'),
            'labels' => $labels,
            'supports' => array('title', 'custom-fields'),
            'hierarchical' => false,
            'public' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'menu_position' => 5,
            'show_in_admin_bar' => true,
            'show_in_nav_menus' => true,
            'can_export' => true,
            'has_archive' => true,
            'exclude_from_search' => false,
            'publicly_queryable' => true,
            'capability_type' => 'post',
            'menu_icon' => 'dashicons-chart-bar',
            'content' => '\0019ff',
            'query_var' => true,
            'rewrite' => array('slug', 'competence'),
        );
        register_post_type('competence', $args);
        flush_rewrite_rules();

    }

    add_action('init', 'aportfolio_cpt', 10);
endif;

