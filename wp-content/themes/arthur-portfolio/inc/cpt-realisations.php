<?php 

// Register Custom Post Type
if ( ! function_exists('arthurportfolio_cpt')) :
function arthurportfolio_cpt() {

	$labels = array(
		'name'                  => _x( 'Réalisations', 'Post Type General Name', 'arthurportfolio' ),
		'singular_name'         => _x( 'realisations', 'Post Type Singular Name', 'arthurportfolio' ),
		'menu_name'             => __( 'Réalisations', 'arthurportfolio' ),
		'name_admin_bar'        => __( 'Realisations', 'arthurportfolio' ),
		'archives'              => __( 'Item Archives', 'arthurportfolio' ),
		'attributes'            => __( 'Item Attributes', 'arthurportfolio' ),
		'parent_item_colon'     => __( 'Parent Item:', 'arthurportfolio' ),
		'all_items'             => __( 'Toutes les realisations', 'arthurportfolio' ),
		'add_new_item'          => __( 'Ajouter une realisation', 'arthurportfolio' ),
		'add_new'               => __( 'Ajouter realisation', 'arthurportfolio' ),
		'new_item'              => __( 'Nouvelle realisation', 'arthurportfolio' ),
		'edit_item'             => __( 'Editer une realisation', 'arthurportfolio' ),
		'update_item'           => __( 'Mettre à jour', 'arthurportfolio' ),
		'view_item'             => __( 'voir realisation', 'arthurportfolio' ),
		'view_items'            => __( 'voir realisation', 'arthurportfolio' ),
		'search_items'          => __( 'Rechercher une realisation', 'arthurportfolio' ),
		'not_found'             => __( 'Aucune realisation', 'arthurportfolio' ),
		'not_found_in_trash'    => __( 'Aucune realisation trouvé dans la corbeille', 'arthurportfolio' ),
		'featured_image'        => __( 'Image à la une', 'arthurportfolio' ),
		'set_featured_image'    => __( 'Parametrer Image à la une', 'arthurportfolio' ),
		'remove_featured_image' => __( 'Supprimer Image à la une', 'arthurportfolio' ),
		'use_featured_image'    => __( 'Utiliser Image à la une', 'arthurportfolio' ),
		'insert_into_item'      => __( 'Insert into item', 'arthurportfolio' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'arthurportfolio' ),
		'items_list'            => __( 'Items list', 'arthurportfolio' ),
		'items_list_navigation' => __( 'Items list navigation', 'arthurportfolio' ),
		'filter_items_list'     => __( 'Filter items list', 'arthurportfolio' ),

	);
	$args = array(
		'label'                 => __( 'realisations', 'arthurportfolio' ),
		'taxonomies' 			=> array('category'),
		'description'           => __( 'CPT pour afficher des fiches des realisations', 'arthurportfolio' ),
		'labels'                => $labels,
		'supports'              => array( 'title','excerpt','author','thumbnail','revisions','custom-fields' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
        'capability_type'       => 'post',
		'menu_icon'             => 'dashicons-category',
		'content'				=> '\0019ff',
        'query_var'             => true,
        'rewrite'               => array( 'slug', 'realisations' ),
	);
        register_post_type( 'realisations', $args );
        flush_rewrite_rules();

}
add_action( 'init', 'arthurportfolio_cpt', 10 );
endif;



// Register Custom Taxonomy

if ( ! function_exists('arthurportfolio_taxonomy')) : 
function arthurportfolio_taxonomy() {


        // Genre de realisation

	$labels_genre = array(
		'name'                       => _x( 'Genre', 'Taxonomy General Name', 'arthurportfolio' ),
		'singular_name'              => _x( 'genre', 'Taxonomy Singular Name', 'arthurportfolio' ),
		'menu_name'                  => __( 'genre', 'arthurportfolio' ),
		'all_items'                  => __( 'All Items', 'arthurportfolio' ),
		'parent_item'                => __( 'Parent Item', 'arthurportfolio' ),
		'parent_item_colon'          => __( 'Parent Item:', 'arthurportfolio' ),
		'new_item_name'              => __( 'New Item Name', 'arthurportfolio' ),
		'add_new_item'               => __( 'Add New Item', 'arthurportfolio' ),
		'edit_item'                  => __( 'Edit Item', 'arthurportfolio' ),
		'update_item'                => __( 'Update Item', 'arthurportfolio' ),
		'view_item'                  => __( 'View Item', 'arthurportfolio' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'arthurportfolio' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'arthurportfolio' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'arthurportfolio' ),
		'popular_items'              => __( 'Popular Items', 'arthurportfolio' ),
		'search_items'               => __( 'Search Items', 'arthurportfolio' ),
		'not_found'                  => __( 'Not Found', 'arthurportfolio' ),
		'no_terms'                   => __( 'No items', 'arthurportfolio' ),
		'items_list'                 => __( 'Items list', 'arthurportfolio' ),
		'items_list_navigation'      => __( 'Items list navigation', 'arthurportfolio' ),
	);
	$args_genre = array(
		'labels'                     => $labels_genre,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
        'query_var'                  => true,
        'rewrite'                    => array( 'slug', 'genre' ),
	);
	
	register_taxonomy( 'genre', array( 'realisations' ), $args_genre );

    $labels_tags = array(
        'name'                       => _x( 'Tags', 'Taxonomy General Name', 'arthurportfolio' ),
        'singular_name'              => _x( 'Tags', 'Taxonomy Singular Name', 'arthurportfolio' ),
        'menu_name'                  => __( 'Tags', 'arthurportfolio' ),
        'all_items'                  => __( 'All Items', 'arthurportfolio' ),
        'parent_item'                => __( 'Parent Item', 'arthurportfolio' ),
        'parent_item_colon'          => __( 'Parent Item:', 'arthurportfolio' ),
        'new_item_name'              => __( 'New Item Name', 'arthurportfolio' ),
        'add_new_item'               => __( 'Ajouter un tag', 'arthurportfolio' ),
        'edit_item'                  => __( 'Edit Item', 'arthurportfolio' ),
        'update_item'                => __( 'Update Item', 'arthurportfolio' ),
        'view_item'                  => __( 'View Item', 'arthurportfolio' ),
        'separate_items_with_commas' => __( 'Separate items with commas', 'arthurportfolio' ),
        'add_or_remove_items'        => __( 'Add or remove items', 'arthurportfolio' ),
        'choose_from_most_used'      => __( 'Choose from the most used', 'arthurportfolio' ),
        'popular_items'              => __( 'Popular Items', 'arthurportfolio' ),
        'search_items'               => __( 'Search Items', 'arthurportfolio' ),
        'not_found'                  => __( 'Not Found', 'arthurportfolio' ),
        'no_terms'                   => __( 'No items', 'arthurportfolio' ),
        'items_list'                 => __( 'Items list', 'arthurportfolio' ),
        'items_list_navigation'      => __( 'Items list navigation', 'arthurportfolio' ),
    );
    $args_tags = array(
        'labels'                     => $labels_tags,
        'hierarchical'               => false,
        'public'                     => true,
        'show_in_rest'               => true,
        'shop_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
        'query_var'                  => true,
        'rewrite'                    => array('slug'=> 'tags'),
    );
    register_taxonomy( 'Tags', array( 'realisations' ), $args_tags );
}
add_action( 'init', 'arthurportfolio_taxonomy', 10 );
endif;
