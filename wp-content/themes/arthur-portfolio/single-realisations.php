<?php
/**
 * The single product page
 *
 * @package WordPress
 * @subpackage arthur-portfolio
 * @since 1.0.0
 */
get_header();
if ( ! function_exists( 'get_field' ) ) {
	return;
}
?>

    <section class="ftco-section">
        <div class="container mt-5">
            <div class="row justify-content-center pb-5">
                <div class="col-md-7 text-center heading-section ftco-animate">
                    <span class="blue"><?php $categories = ' ';
	                    foreach ( get_the_category() as $category ) {
		                    $categories .= $category->name . ', ';
	                    }
	                    echo substr( $categories, 0, - 2 ); ?></span>
                    <h2><?php the_title(); ?></h2>
                </div>
            </div>
            <div class="row d-flex justify-content-center">
                <div class="col-md-8">
                    <h2 class="mb-3"><?php the_field( 'subtitle-1' ); ?></h2>
                    <p><?php the_field( 'text-1' ); ?></p>
					<?php if ( get_field( 'url' ) ): the_field( 'url' ); endif; ?>
                    <p><?php the_field( 'text-2' ); ?></p>

                    <a data-fancybox="images" href="<?php the_post_thumbnail_url(); ?>">
                        <img src="<?php the_post_thumbnail_url(); ?>" width="100%">
                    </a>

                    <div class="d-flex pt-5">
						<?php if ( get_field( 'image-1' ) || get_field( 'image-2' ) || get_field( 'image-3' ) ) : ?>
                            <div class="row">
								<?php if ( get_field( 'image-1' ) ) : ?>
                                    <div class="col">
                                        <a data-fancybox="images" href="<?php the_field( 'image-1' ); ?>">
                                            <img src="<?php the_field( 'image-1' ); ?>" width="100%">
                                        </a>
                                    </div>
								<?php endif; ?>
								<?php if ( get_field( 'image-2' ) ) : ?>
                                    <div class="col">
                                        <a data-fancybox="images" href="<?php the_field( 'image-2' ); ?>">
                                            <img src="<?php the_field( 'image-2' ); ?>" width="100%">
                                        </a>
                                    </div>
								<?php endif; ?>
								<?php if ( get_field( 'image-3' ) ) : ?>
                                    <div class="col">
                                        <a data-fancybox="images" href="<?php the_field( 'image-3' ); ?>">
                                            <img src="<?php the_field( 'image-3' ); ?>" width="100%">
                                        </a>
                                    </div>
								<?php endif; ?>
                            </div>
						<?php endif; ?>
                    </div>

                    <div class="tag-widget post-tag-container mb-5 mt-5">
                        <div class="tagcloud">
							<?php the_terms( '', 'Tags', '', '' ); ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>


<?php
get_footer();
